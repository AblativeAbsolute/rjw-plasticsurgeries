﻿using System.Collections.Generic;
using RimWorld;
using rjw;
using Verse;

namespace RJW_PlasticSurgeries
{
    public abstract class Recipe_Plastic_Surgery : Recipe_Surgery
    {
        protected readonly bool HasLicentia = ModLister.HasActiveModWithName("RimJobWorld - Licentia Labs");

        public override IEnumerable<BodyPartRecord> GetPartsToApplyOn(Pawn pawn, RecipeDef recipe)
        {
            var part = GetPartCandidate(pawn);
            if (part != null)
            {
                var hediffs = Genital_Helper.get_PartsHediffList(pawn, part);
                if (HasPart(pawn, hediffs)) yield return part;
            }
        }

        protected abstract BodyPartRecord GetPartCandidate(Pawn pawn);
        protected abstract bool HasPart(Pawn pawn, List<Hediff> hediffs);

        public override void ApplyOnPawn(Pawn pawn, BodyPartRecord part, Pawn billDoer, List<Thing> ingredients,
            Bill bill)
        {
            if (billDoer != null)
            {
                TaleRecorder.RecordTale(TaleDefOf.DidSurgery, billDoer, pawn);
                SurgeryResult(pawn);
            }
        }

        protected abstract void SurgeryResult(Pawn pawn);

        protected void SurgeryX(Pawn pawn, float severity, bool damagePart = false)
        {
            GetHediffs(pawn).ForEach(hed =>
            {
                hed.Severity = severity;
                if (damagePart && HasLicentia)
                {
                    var (type, damage) = GetLicentiaDamage();
                    LicentiaLabs.DamageHelper.ApplyDamage(pawn, hed.Part, type, damage);
                }
            });
        }

        protected abstract List<Hediff> GetHediffs(Pawn pawn);
        protected abstract (HediffDef, float) GetLicentiaDamage();
    }
}