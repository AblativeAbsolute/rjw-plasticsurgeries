﻿using HugsLib;

namespace RJW_PlasticSurgeries
{
    public class Mod : ModBase
    {
        public override string ModIdentifier => "Stardust3D.RJW.PlasticSurgeries";
    }
}