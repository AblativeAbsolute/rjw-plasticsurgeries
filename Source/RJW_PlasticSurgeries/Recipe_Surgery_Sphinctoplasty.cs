﻿using System;
using System.Collections.Generic;
using LicentiaLabs;
using rjw;
using Verse;

namespace RJW_PlasticSurgeries
{
    public abstract class Recipe_Surgery_Sphinctoplasty : Recipe_Plastic_Surgery
    {
        protected override BodyPartRecord GetPartCandidate(Pawn pawn)
        {
            return Genital_Helper.get_anusBPR(pawn);
        }

        protected override bool HasPart(Pawn pawn, List<Hediff> hediffs)
        {
            return Genital_Helper.has_anus(pawn, hediffs);
        }

        protected override List<Hediff> GetHediffs(Pawn pawn)
        {
            return pawn.GetAnusList();
        }

        protected override (HediffDef, float) GetLicentiaDamage()
        {
            if (HasLicentia)
            {
                return (Licentia.HediffDefs.StretchTear, 0.2f);
            }

            throw new NotSupportedException();
        }
    }

    public class Recipe_Surgery_Sphinctoplasty_Micro : Recipe_Surgery_Sphinctoplasty
    {
        protected override void SurgeryResult(Pawn pawn)
        {
            SurgeryX(pawn, 0.1f, true);
        }
    }

    public class Recipe_Surgery_Sphinctoplasty_Tight : Recipe_Surgery_Sphinctoplasty
    {
        protected override void SurgeryResult(Pawn pawn)
        {
            SurgeryX(pawn, 0.3f, true);
        }
    }

    public class Recipe_Surgery_Sphinctoplasty_Average : Recipe_Surgery_Sphinctoplasty
    {
        protected override void SurgeryResult(Pawn pawn)
        {
            SurgeryX(pawn, 0.5f, true);
        }
    }

    public class Recipe_Surgery_Sphinctoplasty_Accomodating : Recipe_Surgery_Sphinctoplasty
    {
        protected override void SurgeryResult(Pawn pawn)
        {
            SurgeryX(pawn, 0.7f, true);
        }
    }

    public class Recipe_Surgery_Sphinctoplasty_Cavernous : Recipe_Surgery_Sphinctoplasty
    {
        protected override void SurgeryResult(Pawn pawn)
        {
            SurgeryX(pawn, 0.9f, true);
        }
    }
}